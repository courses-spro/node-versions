# Example with Node.js versions

This code demonstrate how different versions of node.js works differently.

To run:
```
$ nvm install v10
$ nvm use v10
$ node index.js
$ nvm install v11
$ nvm use v11
$ node index.js
```

You will see error after last call.
